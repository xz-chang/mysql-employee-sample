# About

This project is based on mysql sample database [*employee*.](https://github.com/datacharmer/test_db) 

## Changes on database schema
I've done my best to avoid changing the table schema, however rails doesn't support composite primary key very well. So `id` columns are added in the tables. Here are some useful scripts for modifying the tables;

```sql
// Drop change primary key
alter table salaries drop primary key, add column id int not null auto_increment primary key first;

// add foreign key
alter table salaries add foreign key(emp_no) references employees(emp_no) on delete cascade

// add unique
alter table titles add constraint uc_title unique(emp_no, title, from_date)
```