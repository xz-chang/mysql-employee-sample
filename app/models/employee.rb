class Employee < ApplicationRecord
  self.primary_key = "emp_no"
  has_many :salaries, foreign_key: :emp_no
  has_many :titles, foreign_key: :emp_no

  has_many :dept_managers, foreign_key: :emp_no
  has_many :in_charge, through: :dept_managers, source: :department 

  has_many :dept_employees, foreign_key: :emp_no
  has_many :departments, through: :dept_employees, source: :department

  scope :gender, -> gender { where(:gender => gender)}
  scope :birthday, -> started_at, ended_at {where(:birth_date => started_at..ended_at)}

  scope :female, -> {gender(:F)}


  validates :first_name, presence: true
  validates :last_name, presence: true

  def authenticate(password)
    true
  end
end
