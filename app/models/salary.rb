class Salary < ApplicationRecord
  belongs_to :employee, foreign_key: 'emp_no'

  scope :current_payment, -> { where("to_date > ?", DateTime.now) }
end
