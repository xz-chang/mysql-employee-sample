class Department < ApplicationRecord
  self.primary_key = "dept_no"
  has_many :dept_managers, foreign_key: :dept_no
  has_many :bosses, through: :dept_managers, source: :employee

  has_many :dept_employees, foreign_key: :dept_no
  has_many :employees, through: :dept_employees
end
