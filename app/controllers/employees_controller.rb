class EmployeesController < ApplicationController
  PP = 100 # Items per page

  has_scope :gender

  has_scope :birthday, :using => [:started_at, :ended_at], :type => :hash 

  def new
  end

  def index
    page = params[:page].to_i
    # Sample URL:
    # http://10.3.0.49:3000/employees?page=20&birthday[started_at]=19000701&birthday[ended_at]=20101013
    @employees = apply_scopes(Employee).order(:emp_no).limit(PP).offset(PP * page)
  end
end
