module Api
  module V1
    class EmployeesController < ActionController::API
      respond_to :json

      PER_PAGE_COUNT = 100
      def index
        page = params[:pp].to_i
        respond_with Employee.order(:emp_no).limit(PER_PAGE_COUNT).offset(page * PER_PAGE_COUNT)
      end

      def show
        respond_with Employee.find_by_emp_no(params[:id])
      end

      def create
        @employee = Employee.new(params.require(:employee).permit(:birth_date, :first_name, :last_name, :gender, :hire_date, :format))
        if @employee.save
          # respond_with @employee
          render json: @employee
        else
          render json: @employee.errors, status: 400
        end
      end

      def destroy
        @employee = Employee.find(params[:id])
        @employee.destroy

        head :ok
      end

    end
  end
end
