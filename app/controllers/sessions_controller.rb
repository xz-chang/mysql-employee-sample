class SessionsController < ApplicationController
  skip_before_action :require_login, only: [:new, :create]

  def new
    session[:return_to] = params[:return_to] if params[:return_to]
  end

  def create
    employee = Employee.find_by_emp_no(params[:emp_no])
    if employee && employee.authenticate(params[:password])
      session[:emp_no] = employee.emp_no
      redirect_to(session.delete(:return_to) || employees_url)
    else
      flash.now.alert = "Employee # is invalid"
      render "new"
    end
  end

  def destroy
    session[:emp_no] = nil
    redirect_to login_url
  end
end
