class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :require_login
  private

  def current_user
    @current_user ||= Employee.find_by_emp_no(session[:emp_no]) if session[:emp_no]
  end 
  helper_method :current_user

  def require_login
    logger.info("====>authorize()")
    redirect_to login_url, alert: "Not authorized" if current_user.nil?
  end
end
