ActiveAdmin.register Salary do
  permit_params :salary, :from_date, :to_date
  belongs_to :employee
  config.filters = false

  scope :current_payment, default:true
  scope :all

  index pagination_total: true, download_links: true do
    selectable_column
    column :Employee do |salary|
      auto_link(salary.employee, "#{salary.employee.first_name}, #{salary.employee.last_name}")
    end
    column :salary
    column :from_date
    column :to_date
  end

  form do |f|
    f.inputs 'Employee details' do
      f.input :salary
      f.input :from_date
      f.input :to_date
    end

    f.actions do
      f.action :submit, button_html: { data: { disable_with: 'Wait...'} }
    end
  end
end
