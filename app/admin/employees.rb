ActiveAdmin.register Employee do
  permit_params :first_name, :last_name, :birth_date, :gender, :hire_date
  scope :all, default:true
  scope :male do |employees| 
    employees.gender(:M)
  end
  scope :female
  
  filter :emp_no
  filter :birth_date
  filter :first_name
  filter :last_name
  filter :gender, as: :select, collection: {:Male => 'M', :Female => 'F'}
  filter :hire_date

  sidebar "Other Info", only: [:show, :edit] do 
    ul do
      li link_to "Salaries", admin_employee_salaries_path(resource)
    end
  end

  index pagination_total: true, download_links: true do 
    selectable_column
    column "Employee number", :emp_no
    column :birth_date
    column "Name" do |employee|
      "#{employee.first_name}, #{employee.last_name}"
    end
#    column :first_name
#    column :last_name
    column :gender do |employee|
      if (employee.gender == "M") 
        :Male
      else
        :Female
      end
    end
    column :hire_date
    actions
  end

  show do
    attributes_table do
      row :emp_no
      row :first_name
      row :last_name
      row :birth_date
      row :gender do |gender|
        if (employee.gender == "M")
          :Male
        else
          :Female
        end
      end
      row :hire_date
    end
  end

  form do |f|
    f.inputs 'Employee details' do
      # You don't want to modify a primary key
      f.input :emp_no, input_html: { disabled: true }
      f.input :first_name
      f.input :last_name
      f.input :birth_date, as: :datepicker
      f.input :gender, as: :select, collection: {:Male => 'M', :Female => 'F'}, :include_blank => false
      f.input :hire_date, as: :datepicker
    end

    f.actions do
      f.action :submit, button_html: { data: { disable_with: 'Wait...'} }
    end
  end
end
