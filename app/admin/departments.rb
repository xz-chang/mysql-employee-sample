ActiveAdmin.register Department do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  # config.filters = false
  permit_params :dept_name, :dept_no
  filter :dept_no
  filter :dept_name


  form do |f|
    f.inputs 'Department details' do
      f.input :dept_no
      f.input :dept_name
    end

    f.actions do
      f.action :submit, button_html: { data: { disable_with: 'Wait...'}}
    end
  end

  show do
    attributes_table do
      row :dept_no
      row :dept_name
    end
  end
end
