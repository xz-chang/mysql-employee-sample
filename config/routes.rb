Rails.application.routes.draw do
  ActiveAdmin.routes(self)

  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :employees, only: [:index]
  resources :sessions

  # Example request: http://10.3.0.49:3000/filter-employees-by-gender/m
  get '/filter-employees-by-gender/:gender' => 'employees#index'

  namespace :api, defaults:{format: 'json'} do
    namespace :v1 do
      resources :employees
    end
  end
end
